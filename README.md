<p><h2>İçerik Yönetim Sistemi</h2></p>
<b>Amacı</b><p>Web sitelerinin kontrol panelinden yönetilmesi</p>
<b>Kullanılan Teknolojiler</b>
<ul>
<li>asp.net mvc, katmanlı mimari, OOP, Dependency Injection(Autofac)</li>
<li>mssql, entity framework code first</li>
<li>html5, css3, jquery, angular</li>
<li>signalR(Şuan için sadece kontrol panelinde product güncellendiği zaman gerçek zamanlı olarak son kullanıcıya gösteriliyor.)</li>
</ul>
<b>Katmanlar</b>
<ul>
<li><b>Data:</b> Veri tabanı modelleri ve context bulunur.</li>
<li><b>Console:</b> Data katmanındaki modellere göre veri tabanını ayağa kaldırmak için kullanılmıştır.</li>
<li><b>Core:</b> CRUD operasyonları yapılmıştır.</li>
<li><b>Web:</b> Admin ve UI kısmı burada bulunur.</li>
<li><b>Service:</b> Yardımcı methotlar</li>

</ul>
<b>Gerekli Programlar</b>
<ul>
<li>Visual Studio 2015</li>
<li>SQL Server 2014</li>
</ul>

<b>Local de Çalıştırmak İçin</b>
<ul>
<li>DWTCMS.Console projesini başlangıç projesi olarak ayarlayın, starta basarak veri tabanının oluşmasını bekleyin</li>
<li>SQL Server da default.sql çalıştırın.</li>
<li>DWTCMS.Web projesini başlangıç projesi olarak ayarlayın.Uygulamayı başlatın.</li>
<li>Admin paneli giriş için E-mail: info@demirwebtasarim.net - Şifre: root</li>