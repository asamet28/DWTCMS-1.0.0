﻿using DWTCMS.Data.Model.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DWTCMS.Core.Repository;
using DWTCMS.Service.Config;

namespace DWTCMS.Service.Settings
{

    /// <summary>
    /// Get or Set Application Settings
    /// </summary>
    // ReSharper disable once CheckNamespace
    public static class AppSettings
    {
        /// <summary>
        /// Appends configuration site name to given text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        //public static string BuildDocumentTitle(this string text)
        //{
        //    var siteName = SiteName;

        //    if (text.IsNullOrEmpty())
        //    {
        //        return siteName;
        //    }

        //    if (siteName.IsNullOrEmpty())
        //    {
        //        return text;
        //    }


        //    return text + " - " + SiteName;
        //}


        /// <summary>
        /// Get application site uri.
        /// </summary>
        //public static string Url
        //{
        //    get
        //    {
        //        return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
        //    }
        //}


        /// <summary>
        /// Get application site uri.
        /// </summary>
        public static string SiteName
        {
            get { return Get(AppCons.TITLE); }
        }

        /// <summary>
        /// Get key value.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Get(string key)
        {
            Setting data;
            try
            {
                data = new SettingRepository().Get(x => x.Key.Equals(key));
            }
            catch
            {
                data = null;
            }

            return data == null ? "" : data.Value;
        }




        /// <summary>
        /// Insert a new record or update it.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public static void Set(string key, string val)
        {
            var settingData = new SettingRepository();

            var setting = settingData.Get(x => x.Key.Equals(key));
            if (setting != null)
            {
                setting.Value = val;
                settingData.Update(setting);
            }
            else
            {
                settingData.Insert(new Setting { Key = key, Value = val });

            }
            settingData.Save();
        }

    }
}
