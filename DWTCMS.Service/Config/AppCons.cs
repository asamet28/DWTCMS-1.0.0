﻿namespace DWTCMS.Service.Config
{
    public static class AppCons
    {
        public const string NOINDEX = "NOINDEX";
        public const string HeadCode = "HeadCode";
        public const string BodyCode = "BodyCode";

        // SALES SETTINGS
        public const string EMAIL_REGEX = @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
        public const int CV_FILE_MAX_SIZE = 3;

        public const string ContactFormResult = "ContactFormResult";


        // Login cookie names
        public const string AdminLoginCookie = "_uax";
        public const string SupplierLoginCookie = "_usx";
        public const string UserLoginCookie = "_uex";
        public const string PdfCookie = "_pdf";

        public const string UserBasketCookie = "_b";
        public const string ProductSlugPrefix = "liste";
        public const string BannersTitle = "BannersTitle";


        // SETTINGS
        public const string PANEL_LOGO = "PANEL_LOGO";
        public const string TITLE = "TITLE";
        public const string SITE_NAME = "SITE_NAME";
        public const string KEYWORDS = "KEYWORDS";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string GOOGLE_ANALYTICS_CODE = "GOOGLE_ANALYTICS_CODE";
        public const string FOOTER_TEXT = "FOOTER_TEXT";

        public const string MAIL_SERVER = "MAIL_SERVER";
        public const string MAIL_USER = "MAIL_USER";
        public const string MAIL_PASSWORD = "MAIL_PASSWORD";
        public const string MAIL_SENDER_NAME = "MAIL_SENDER_NAME";
        public const string MAIL_NOTIFICATIONS = "MAIL_NOTIFICATIONS";

        public const string FACEBOOK_PAGE_URL = "FACEBOOK_PAGE_URL";
        public const string TWITTER_PAGE_URL = "TWITTER_PAGE_URL";
        public const string GOOGLE_PLUS_PAGE_URL = "GOOGLE_PLUS_PAGE_URL";
        public const string INSTAGRAM_PAGE_URL = "INSTAGRAM_PAGE_URL";
        public const string PINTEREST_PAGE_URL = "PINTEREST_PAGE_URL";
        public const string LINKEDIN_PAGE_URL = "LINKEDIN_PAGE_URL";
        public const string YOUTUBE_PAGE_URL = "YOUTUBE_PAGE_URL";

        public const string FACEBOOK_PAGE_URL_ISOPEN = "FACEBOOK_PAGE_URL_ISOPEN";
        public const string TWITTER_PAGE_URL_ISOPEN = "TWITTER_PAGE_URL_ISOPEN";
        public const string GOOGLE_PLUS_PAGE_URL_ISOPEN = "GOOGLE_PLUS_PAGE_URL_ISOPEN";
        public const string INSTAGRAM_PAGE_URL_ISOPEN = "INSTAGRAM_PAGE_URL_ISOPEN";
        public const string PINTEREST_PAGE_URL_ISOPEN = "PINTEREST_PAGE_URL_ISOPEN";
        public const string LINKEDIN_PAGE_URL_ISOPEN = "LINKEDIN_PAGE_URL_ISOPEN";
        public const string YOUTUBE_PAGE_URL_ISOPEN = "YOUTUBE_PAGE_URL_ISOPEN";

        public const string ADRES = "ADRES";
        public const string TELEFON = "TELEFON";
        public const string MAIL = "MAIL";
        public const string FAX = "FAX";
        public const string SAATLER = "SAATLER";


        public const string CONFIRMATION_MESSAGE = "CONFIRMATION_MESSAGE";
        public const string CONFIRMATION_ISOPEN = "CONFIRMATION_ISOPEN";


        // GLOBAL MESSAGES
        public const string GlobalAnErrorMessage = "Bir hata oluştu. Lütfen tekrar deneyiniz.";
        public const string UploadRootFolderName = "upload";
        public const string Required = "Bu alanın doldurulması zorunludur.";
        public const string EmailRequired = "Lütfen geçerli bir e-posta adresi giriniz.";


        // TEMP DATA KEYS

        public const string TempDataSuccess = "TempDataSuccess";
        public const string TempDataError = "TempDataError";
        public const string TempDataInfo = "TempDataInfo";


        // TEMP DATA KEYS

        public const string PasswordChanged = "PasswordChanged";
        public const string AccountEdited = "AccountEdited";

        public const string ErrorList = "ErrorList";
        public const string ProcessSuccess = "ProcessSuccess";



        public const string reCAPTCHA_SiteKey = "reCAPTCHA_SiteKey";
        public const string reCAPTCHA_SecretKey = "reCAPTCHA_SecretKey";

        public const string FacebookAppId = "184078831984673";
        public const string FacebookAppSecret = "9b38688e7a7135c5c8d7d616accebead";
        public const string FacebookAppScope = "public_profile"; // public_profile, email

    }
}
