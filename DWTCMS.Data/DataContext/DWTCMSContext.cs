﻿using DWTCMS.Data.Model;
using DWTCMS.Data.Model.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.DataContext
{
    public class DWTCMSContext : DbContext
    {
        public DbSet<Admin> Admin { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<CmsPage> CmsPage { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Slide> Slide { get; set; }
        public DbSet<Image> Image { get; set; }
        public DbSet<Setting> Setting { get; set; }
        public DbSet<Translate> Translate { get; set; }
        public DbSet<Error> Error { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();            
        }
    }
}