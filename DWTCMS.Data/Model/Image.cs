﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model
{
    public class Image : BaseEntity.BaseEntity
    {
        public string ImageName { get; set; }
        public string ContentType { get; set; }
        
        public string Path { get; set; }
        public int? ProductID { get; set; }
        public virtual Product Product { get; set; }
        
    }
}
