﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model
{
    public class Post : BaseEntity.BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime AddedDateTime { get; set; }

        public bool IsOpen { get; set; }
        public int Order { get; set; }

        public string SeoName { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        public virtual ICollection<Translate> Translate { get; set; }
    }
}
