﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model.Settings
{
    public class Setting : BaseEntity.BaseEntity
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
