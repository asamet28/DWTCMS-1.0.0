﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model.Settings
{
    public class Error : BaseEntity.BaseEntity
    {
        public string StringErrorData { get; set; }
        public bool IsViewed { get; set; }
    }
}
