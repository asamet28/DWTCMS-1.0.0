﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace DWTCMS.Data.Model
{
    public class Category : BaseEntity.BaseEntity
    {
        public string CategoryTurkishName { get; set; }
        public string CategoryEnglishName { get; set; }
        public string CategoryRussianName { get; set; }
        public string CategoryArabicName { get; set; }

        public int Order { get; set; }
        public bool IsOpen { get; set; }

        public string SeoName { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        public int? ParentCategoryId { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Translate> Translate { get; set; }
    }
}
