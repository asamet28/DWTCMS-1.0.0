﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model
{
    public class CmsPage : BaseEntity.BaseEntity
    {
        public string PageTitle { get; set; }
        public string PageShortDescription { get; set; }
        public string PageDescription { get; set; }
        public string Image { get; set; }
        public PageValue PageValue { get; set; }

        public string MetaKeyword { get; set; }

        public virtual ICollection<Translate> Translate { get; set; }
    }

    public enum PageValue
    {
        Home,
        About,
        Documents,
        News,
        Contact,
    }
}
