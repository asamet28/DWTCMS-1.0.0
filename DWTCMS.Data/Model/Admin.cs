﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model
{
    public class Admin:BaseEntity.BaseEntity
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool? isChecked { get; set; }
        public Guid? GuidID { get; set; }
    }
}
