﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model
{
    public class Translate : BaseEntity.BaseEntity
    {        
        public LanguageCode LanguageCode { get; set; }
        public string Name { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        public int? ProductID { get; set; }
        public int? CategoryID { get; set; }
        public int? PostID { get; set; }
        public int? SlideID { get; set; }
        public int? CmsPageID { get; set; }

        public virtual Product Product { get; set; }
    }
    public enum LanguageCode
    {
        en,
        ru,
        ar,
    }
}
