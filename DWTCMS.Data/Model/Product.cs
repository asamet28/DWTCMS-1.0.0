﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace DWTCMS.Data.Model
{
    public class Product : BaseEntity.BaseEntity
    {
        public string Code { get; set; }

        public string ProductTurkishName { get; set; }
        public string ProductEnglishName { get; set; }
        public string ProductRussianName { get; set; }
        public string ProductArabicName { get; set; }

        public int CategoryId { get; set; }

        public bool IsOpen { get; set; }
        public int Order { get; set; }

        public string SeoName { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Translate> Translate { get; set; }
    }
}
