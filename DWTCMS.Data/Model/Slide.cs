﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Data.Model
{
    public class Slide : BaseEntity.BaseEntity
    {
        public string Title { get; set; }

        public string Path { get; set; }
        public string ImageName { get; set; }

        public bool IsOpen { get; set; }
        public int Order { get; set; }

        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        public virtual ICollection<Translate> Translate { get; set; }
    }
}
