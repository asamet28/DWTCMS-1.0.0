﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Repository
{
   public class PostRepository : IPostRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Post.Count();
        }

        public void Delete(int id)
        {
            var post = GetById(id);
            if (post != null)
            {
                _context.Post.Remove(post);
            }
        }

        public Post Get(Expression<Func<Post, bool>> expression)
        {
            return _context.Post.FirstOrDefault(expression);
        }

        public IEnumerable<Post> GetAll()
        {
            return _context.Post.Select(x => x);
        }

        public Post GetById(int id)
        {
            return _context.Post.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Post, bool>> expression)
        {
            return _context.Post.Where(expression);
        }

        public void Insert(Post obj)
        {
            _context.Post.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Post obj)
        {
            _context.Post.AddOrUpdate(obj);
        }
    }
}
