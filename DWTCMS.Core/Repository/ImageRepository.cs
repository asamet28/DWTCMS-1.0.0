﻿using DWTCMS.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DWTCMS.Data.Model;
using System.Linq.Expressions;
using DWTCMS.Data.DataContext;
using System.Data.Entity.Migrations;

namespace DWTCMS.Core.Repository
{
    public class ImageRepository : IImageRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Image.Count();
        }

        public void Delete(int id)
        {
            var image = GetById(id);
            if (image != null)
            {
                _context.Image.Remove(image);
            }
        }

        public Image Get(Expression<Func<Image, bool>> expression)
        {
            return _context.Image.FirstOrDefault(expression);
        }

        public IEnumerable<Image> GetAll()
        {
            return _context.Image.Select(x => x);
        }

        public Image GetById(int id)
        {
            return _context.Image.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Image, bool>> expression)
        {
            return _context.Image.Where(expression);
        }

        public void Insert(Image obj)
        {
            _context.Image.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Image obj)
        {
            _context.Image.AddOrUpdate(obj);
        }
    }
}
