﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Repository
{
   public class SettingRepository : ISettingRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Setting.Count();
        }

        public void Delete(int id)
        {
            var setting = GetById(id);
            if (setting != null)
            {
                _context.Setting.Remove(setting);
            }
        }

        public Setting Get(Expression<Func<Setting, bool>> expression)
        {
            return _context.Setting.FirstOrDefault(expression);
        }

        public IEnumerable<Setting> GetAll()
        {
            return _context.Setting.Select(x => x);
        }

        public Setting GetById(int id)
        {
            return _context.Setting.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Setting, bool>> expression)
        {
            return _context.Setting.Where(expression);
        }

        public void Insert(Setting obj)
        {
            _context.Setting.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Setting obj)
        {
            _context.Setting.AddOrUpdate(obj);
        }
    }
}
