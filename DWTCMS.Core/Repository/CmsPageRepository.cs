﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Repository
{
   public class CmsPageRepository : ICmsPageRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.CmsPage.Count();
        }

        public void Delete(int id)
        {
            var cmsPage = GetById(id);
            if (cmsPage != null)
            {
                _context.CmsPage.Remove(cmsPage);
            }
        }

        public CmsPage Get(Expression<Func<CmsPage, bool>> expression)
        {
            return _context.CmsPage.FirstOrDefault(expression);
        }

        public IEnumerable<CmsPage> GetAll()
        {
            return _context.CmsPage.Select(x => x);
        }

        public CmsPage GetById(int id)
        {
            return _context.CmsPage.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<CmsPage, bool>> expression)
        {
            return _context.CmsPage.Where(expression);
        }

        public void Insert(CmsPage obj)
        {
            _context.CmsPage.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(CmsPage obj)
        {
            _context.CmsPage.AddOrUpdate(obj);
        }
    }
}
