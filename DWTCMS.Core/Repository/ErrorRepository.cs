﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Repository
{
   public class ErrorRepository : IErrorRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Error.Count();
        }

        public void Delete(int id)
        {
            var error = GetById(id);
            if (error != null)
            {
                _context.Error.Remove(error);
            }
        }

        public Error Get(Expression<Func<Error, bool>> expression)
        {
            return _context.Error.FirstOrDefault(expression);
        }

        public IEnumerable<Error> GetAll()
        {
            return _context.Error.Select(x => x);
        }

        public Error GetById(int id)
        {
            return _context.Error.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Error, bool>> expression)
        {
            return _context.Error.Where(expression);
        }

        public void Insert(Error obj)
        {
            _context.Error.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Error obj)
        {
            _context.Error.AddOrUpdate(obj);
        }
    }
}
