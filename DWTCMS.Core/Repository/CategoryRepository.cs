﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;



namespace DWTCMS.Core.Repository
{
    public class CategoryRepository :ICategoryRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Category.Count();
        }

        public void Delete(int id)
        {
            var category = GetById(id);
            if (category != null)
            {
                _context.Category.Remove(category);
            }
        }

        public Category Get(Expression<Func<Category, bool>> expression)
        {
            return _context.Category.FirstOrDefault(expression);
        }

        public IEnumerable<Category> GetAll()
        {
            return _context.Category.Select(x => x);
        }

        public Category GetById(int id)
        {
            return _context.Category.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Category, bool>> expression)
        {
            return _context.Category.Where(expression);
        }

        public void Insert(Category obj)
        {
            _context.Category.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Category obj)
        {
           _context.Category.AddOrUpdate(obj);
        }
    }
}
