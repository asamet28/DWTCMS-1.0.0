﻿using DWTCMS.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DWTCMS.Data.Model;
using System.Linq.Expressions;
using DWTCMS.Data.DataContext;
using System.Data.Entity.Migrations;

namespace DWTCMS.Core.Repository
{
    public class SlideRepository : ISlideRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Slide.Count();
        }

        public void Delete(int id)
        {
            var slide = GetById(id);
            if (slide != null)
            {
                _context.Slide.Remove(slide);
            }
        }

        public Slide Get(Expression<Func<Slide, bool>> expression)
        {
            return _context.Slide.FirstOrDefault(expression);
        }

        public IEnumerable<Slide> GetAll()
        {
            return _context.Slide.Select(x => x);
        }

        public Slide GetById(int id)
        {
            return _context.Slide.FirstOrDefault(x => x.ID == id);
        }
       

        public IQueryable GetMany(Expression<Func<Slide, bool>> expression)
        {
            return _context.Slide.Where(expression);
        }

        public void Insert(Slide obj)
        {
            _context.Slide.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Slide obj)
        {
            _context.Slide.AddOrUpdate(obj);
        }
    }
}
