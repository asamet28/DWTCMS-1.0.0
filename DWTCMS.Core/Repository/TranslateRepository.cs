﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Repository
{
   public class TranslateRepository : ITranslateRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Translate.Count();
        }

        public void Delete(int id)
        {
            var translate = GetById(id);
            if (translate != null)
            {
                _context.Translate.Remove(translate);
            }
        }

        public Translate Get(Expression<Func<Translate, bool>> expression)
        {
            return _context.Translate.FirstOrDefault(expression);
        }

        public IEnumerable<Translate> GetAll()
        {
            return _context.Translate.Select(x => x);
        }

        public Translate GetById(int id)
        {
            return _context.Translate.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Translate, bool>> expression)
        {
            return _context.Translate.Where(expression);
        }

        public void Insert(Translate obj)
        {
            _context.Translate.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Translate obj)
        {
            _context.Translate.AddOrUpdate(obj);
        }
    }
}
