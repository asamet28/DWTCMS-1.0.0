﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Repository
{
   public class AdminRepository : IAdminRepository
    {
        private readonly DWTCMSContext _context = new DWTCMSContext();
        public int Count()
        {
            return _context.Admin.Count();
        }

        public void Delete(int id)
        {
            var admin = GetById(id);
            if (admin != null)
            {
                _context.Admin.Remove(admin);
            }
        }

        public Admin Get(Expression<Func<Admin, bool>> expression)
        {
            return _context.Admin.FirstOrDefault(expression);
        }

        public IEnumerable<Admin> GetAll()
        {
            return _context.Admin.Select(x => x);
        }

        public Admin GetById(int id)
        {
            return _context.Admin.FirstOrDefault(x => x.ID == id);
        }

        public IQueryable GetMany(Expression<Func<Admin, bool>> expression)
        {
            return _context.Admin.Where(expression);
        }

        public void Insert(Admin obj)
        {
            _context.Admin.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Admin obj)
        {
            _context.Admin.AddOrUpdate(obj);
        }
    }
}
