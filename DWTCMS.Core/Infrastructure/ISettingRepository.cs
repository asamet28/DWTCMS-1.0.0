﻿using DWTCMS.Data.Model;
using DWTCMS.Data.Model.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWTCMS.Core.Infrastructure
{
   public interface ISettingRepository : IRepository<Setting>
    {

    }
}
