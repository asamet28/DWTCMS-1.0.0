﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.Model;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DWTCMS.Data.DataContext;
using System.IO;
using System.Web.Routing;
using DWTCMS.Web.Models;

namespace DWTCMS.Web.Controllers
{
    public class HomeController : Controller
    {        
        private readonly ICategoryRepository _categoryRepository;
        private readonly IProductRepository _productRepository;
        private readonly ISlideRepository _slideRepository;
        private readonly ICmsPageRepository _cmsPageRepository;
        private readonly IPostRepository _postRepository;
        public HomeController(ICategoryRepository categoryRepository, IProductRepository productRepository, ISlideRepository slideRepository, ICmsPageRepository cmsPageRepository, IPostRepository postRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _slideRepository = slideRepository;
            _cmsPageRepository = cmsPageRepository;
            _postRepository = postRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult _Slide()
        {
            var model = _slideRepository.GetAll().ToList();
            return PartialView(model);
        }
       
        public JsonResult GetAllProduct()
        {
            var model = _productRepository.GetAll().ToList();
            var viewModel = new List<HomeProduct>();

            if (model.Any())
            {
                foreach (var p in model)
                {
                    var vm = new HomeProduct
                    {
                        trName = p.ProductTurkishName,
                        MetaDescription = p.MetaDescription,
                        Code = p.Code
                    };

                    viewModel.Add(vm);
                }
            }
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
        public class dWTCMSHub : Hub
        {

        }
        public PartialViewResult _Blog()
        {
            var model = _postRepository.GetAll().ToList();
            return PartialView(model);
        }
        public PartialViewResult _Reference()
        {
            var model = _productRepository.GetAll().ToList().Take(6);
            return PartialView(model);
        }
        public ActionResult AboutPage()
        {
            var model = _cmsPageRepository.Get(x => x.PageValue == PageValue.About);
            return View(model);
        }
        public ActionResult BlogPage()
        {
            var model = _postRepository.GetAll().ToList();
            return View(model);
        }
        public ActionResult BlogDetail(int id)
        {
            var model = _postRepository.GetById(id);
            return View(model);
        }
    }   
}
