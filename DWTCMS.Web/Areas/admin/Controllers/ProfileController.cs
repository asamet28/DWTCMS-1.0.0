﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Web.Areas.admin.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DWTCMS.Data.Model;
using DWTCMS.Service.Crypt;
using DWTCMS.Service.Config;
using DWTCMS.Web.Areas.admin.ViewModel.Admin;

namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class ProfileController : Controller
    {
        private readonly IAdminRepository _adminRepository;

        public ProfileController(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        public ActionResult Index()
        {
            var user = _adminRepository.Get(x => x.Email.Equals(User.Identity.Name));
            AdminProfile model = new AdminProfile();
            model.Name = user.Name;
            model.Email = user.Email;
            return View(model);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangPasswordAdmin resetPassword)
        {
            if (ModelState.IsValid)
            {
                var user = _adminRepository.Get(x => x.Email.Equals(User.Identity.Name));

                if (BCrypt.CheckPassword(resetPassword.Password, user.Password))
                {
                    user.Password = BCrypt.HashPassword(resetPassword.NewPassword, BCrypt.GenerateSalt());
                    try
                    {
                        _adminRepository.Update(user);
                        _adminRepository.Save();
                        TempData[AppCons.TempDataSuccess] = "Şifreniz başarıyla değiştirildi";
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                else
                {
                    TempData[AppCons.TempDataError] = "Şifreniz doğru girilmedi!";
                }
            }

            return RedirectToAction("Index");

        }
    }
}