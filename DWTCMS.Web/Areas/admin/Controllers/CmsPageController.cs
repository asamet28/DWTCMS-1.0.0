﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.Model;
using DWTCMS.Service.Config;
using DWTCMS.Web.Areas.admin.ViewModel;
using DWTCMS.Web.Areas.admin.ViewModel.Admin.CmsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class CmsPageController : Controller
    {
        private readonly ICmsPageRepository _cmsPageRepository;
        public CmsPageController(ICmsPageRepository cmsPageRepository)
        {
            _cmsPageRepository = cmsPageRepository;
        }
        public ActionResult Index()
        {
            var model = _cmsPageRepository.GetAll().ToList();

            var viewModel = new List<CmsPageList>();
            if (model.Any())
            {
                foreach (var item in model)
                {
                    var vm = new CmsPageList
                    {
                        Id = item.ID,
                        PageValue = item.PageValue.ToString()
                    };
                    viewModel.Add(vm);
                }
            }
            return View(viewModel);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                TempData[AppCons.TempDataError] = "İçerik sayfası bulunamadı";
                return RedirectToAction("Index");
            }
            var cmsPage = _cmsPageRepository.GetById(id.Value);
            if (cmsPage==null)
            {
                TempData[AppCons.TempDataError] = "İçerik sayfası bulunamadı";
                return RedirectToAction("Index");
            }
            var uModel = new CmsPageEdit
            {
                PageDescription = cmsPage.PageDescription,
                PageShortDescription = cmsPage.PageShortDescription,
                PageValue = (int)cmsPage.PageValue
            };
            return View(uModel);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(CmsPageEdit model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            CmsPage uModel = new CmsPage()
            {
                PageDescription = model.PageDescription,
                PageShortDescription = model.PageShortDescription,
                ID = model.ID,
                PageValue = (PageValue)model.PageValue
            };
            try
            {
                _cmsPageRepository.Update(uModel);
                _cmsPageRepository.Save();


                TempData[AppCons.TempDataSuccess] = "İçerik sayfası başarıyla düzenlendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
    }
}