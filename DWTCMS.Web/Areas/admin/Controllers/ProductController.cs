﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.Model;
using DWTCMS.Service.Config;
using DWTCMS.Web.Areas.admin.Helpers;
using DWTCMS.Web.Areas.admin.ViewModel;
using DWTCMS.Web.Areas.admin.ViewModel.Admin.Category;
using DWTCMS.Web.Areas.admin.ViewModel.Admin.Product;
using DWTCMS.Web.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IImageRepository _imageRepository;
        private readonly ITranslateRepository _translateRepository;

        public ProductController(IProductRepository productRepository, ICategoryRepository categoryRepository, IImageRepository imageRepository, ITranslateRepository translateRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _imageRepository = imageRepository;
            _translateRepository = translateRepository;
        }

        public ActionResult Index()
        {
            var model = _productRepository.GetAll().ToList().Select(x => new { x.ID, x.ProductTurkishName, x.Code, x.Category.CategoryTurkishName, x.Order, x.IsOpen, x.Images }).OrderBy(y => y.Order);

            var viewModel = new List<ProductList>();

            if (model.Any())
            {
                foreach (var p in model)
                {
                    var vm = new ProductList
                    {
                        ID = p.ID,
                        Category = p.CategoryTurkishName,
                        Code = p.Code,
                        Order = p.Order,
                        IsOpen = p.IsOpen,
                        TName = p.ProductTurkishName,
                        ImgPath = p.Images.FirstOrDefault().Path
                    };

                    viewModel.Add(vm);
                }
            }
            return View(viewModel);
        }

        public ActionResult Create()
        {
            #region SelectListCategory
            var model = SortCategoriesForTree(_categoryRepository.GetAll().OrderBy(x => x.ParentCategoryId).ThenBy(y => y.Order).ToList());
            var viewModel = new List<CategoryCreateOrEdit>();
            if (model.Any())
            {
                foreach (var item in model)
                {
                    var vm = new CategoryCreateOrEdit
                    {
                        ID = item.ID,
                        Order = item.Order,
                        IsOpen = item.IsOpen,
                        Name = GetFormattedBreadCrumb(item)
                    };
                    viewModel.Add(vm);
                }
            }
            object selectCategory = null;
            var selectList = new SelectList(viewModel, "Id", "Name", selectCategory);
            ViewData.Add("ParentCategoryId", selectList);
            #endregion

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductCreateOrEdit model, HttpPostedFileBase productImage)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (productImage == null || productImage.ContentLength <= 0)
            {
                TempData[AppCons.TempDataError] = "Resim eklemelisiniz";
                #region SelectListCategory
                var modell = SortCategoriesForTree(_categoryRepository.GetAll().OrderBy(x => x.ParentCategoryId).ThenBy(y => y.Order).ToList());
                var viewModel = new List<CategoryCreateOrEdit>();
                if (modell.Any())
                {
                    foreach (var item in modell)
                    {
                        var vm = new CategoryCreateOrEdit
                        {
                            ID = item.ID,
                            Order = item.Order,
                            IsOpen = item.IsOpen,
                            Name = GetFormattedBreadCrumb(item)
                        };
                        viewModel.Add(vm);
                    }
                }
                object selectCategory = null;
                var selectList = new SelectList(viewModel, "Id", "Name", selectCategory);
                ViewData.Add("ParentCategoryId", selectList);
                #endregion

                return View(model);
            }
            var cModel = new Product
            {
                ProductTurkishName = model.trName,
                Code = model.Code,
                CategoryId = model.ParentCategoryId,
                IsOpen = model.IsOpen,
                Order = model.Order,
                MetaDescription = model.MetaDescription,
                MetaKeyword = model.MetaKeyword,
                SeoName = model.trName.Trim().GetGlobalSeoUrlFiendlyName()
            };
            var enModel = new Translate
            {
                Name = model.enName,
                LanguageCode = LanguageCode.en
            };

            string guid = string.Empty;
            guid = Guid.NewGuid().ToString();
            productImage.SaveAs(Path.Combine(Server.MapPath("~/Areas/admin/Content/img/product/"), guid + productImage.FileName));
            var img = new Image
            {
                Path = guid + productImage.FileName
            };
            cModel.Images = new List<Image> { img };

            try
            {
                _productRepository.Insert(cModel);
                _productRepository.Save();
                enModel.ProductID = cModel.ID;
                _translateRepository.Insert(enModel);
                _translateRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Ürün başarıyla eklendi";
            }
            catch (Exception)
            {
                TempData[AppCons.TempDataError] = "Ürün eklenirken bir sorun oluştu";
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            #region SelectListCategory select listi doldurmak için
            var model = SortCategoriesForTree(_categoryRepository.GetAll().OrderBy(x => x.ParentCategoryId).ThenBy(y => y.Order).ToList());
            var viewModel = new List<CategoryCreateOrEdit>();
            if (model.Any())
            {
                foreach (var item in model)
                {
                    var vm = new CategoryCreateOrEdit
                    {
                        ID = item.ID,
                        Order = item.Order,
                        IsOpen = item.IsOpen,
                        Name = GetFormattedBreadCrumb(item)
                    };
                    viewModel.Add(vm);
                }
            }
            #endregion

            var product = _productRepository.GetById(id.Value);
            if (product == null)
            {
                return HttpNotFound();
            }
            ProductCreateOrEdit uModel = new ProductCreateOrEdit()
            {
                ID = product.ID,
                trName = product.ProductTurkishName,
                Code = product.Code,
                ImgPath = product.Images.Select(x => x.Path).FirstOrDefault(),
                IsOpen = product.IsOpen,
                MetaDescription = product.MetaDescription,
                MetaKeyword = product.MetaKeyword,
                Order = product.Order,
                enName = product.Translate.Where(x => x.LanguageCode == LanguageCode.en).Select(y => y.Name).FirstOrDefault()
            };

            #region SelectListCategory Seçili Kategoriyi göstermek için
            var selectList = new SelectList(viewModel, "Id", "Name", product.CategoryId);
            ViewData.Add("ParentCategoryId", selectList);
            #endregion

            return View(uModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductCreateOrEdit product, HttpPostedFileBase productImage)
        {
            Product uModel = new Product()
            {
                ID = product.ID,
                ProductTurkishName = product.trName,
                Code = product.Code,
                CategoryId = product.ParentCategoryId,
                IsOpen = product.IsOpen,
                Order = product.Order,
                MetaDescription = product.MetaDescription,
                MetaKeyword = product.MetaKeyword,
                SeoName = product.trName.Trim().GetGlobalSeoUrlFiendlyName(),
            };
            var model = _translateRepository.GetAll().Where(x => x.ProductID == product.ID).ToList();
            var enModel = new Translate
            {
                ID = model.Where(x => x.LanguageCode == LanguageCode.en).Select(y => y.ID).FirstOrDefault(),
                Name = product.enName,
                LanguageCode = LanguageCode.en,
                ProductID = product.ID
            };
            string guid = string.Empty;
            if (productImage != null && productImage.ContentLength > 0)
            {
                var delProduct = _productRepository.GetById(product.ID);
                foreach (var item in delProduct.Images)
                {
                    System.IO.File.Delete(Server.MapPath("~/Areas/admin/Content/img/product/" + item.Path));
                    _imageRepository.Delete(item.ID);
                }
                _imageRepository.Save();

                guid = Guid.NewGuid().ToString();
                productImage.SaveAs(Path.Combine(Server.MapPath("~/Areas/admin/Content/img/product/"), guid + productImage.FileName));
                var img = new Image
                {
                    Path = guid + productImage.FileName,
                    ProductID = product.ID

                };
                _imageRepository.Insert(img);
                _imageRepository.Save();
            }
            try
            {
                _translateRepository.Update(enModel);
                _translateRepository.Save();
                _productRepository.Update(uModel);
                _productRepository.Save();
                #region Gerçek zamanlı client tüm dataları yollar.
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<Web.Controllers.HomeController.dWTCMSHub>();
                var data = _productRepository.GetAll();
                var viewModel = new List<HomeProduct>();

                if (data.Any())
                {
                    foreach (var p in data)
                    {
                        var vm = new HomeProduct
                        {
                            trName = p.ProductTurkishName,
                            MetaDescription = p.MetaDescription,
                            Code = p.Code
                        };

                        viewModel.Add(vm);
                    }
                }
                hubContext.Clients.All.RefreshProduct(viewModel);
                #endregion
                TempData[AppCons.TempDataSuccess] = "Ürün başarıyla düzenlendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            try
            {
                var product = _productRepository.GetById(id.Value);
                foreach (var item in product.Images)
                {
                    System.IO.File.Delete(Server.MapPath("~/Areas/admin/Content/img/product/" + item.Path));
                    _imageRepository.Delete(item.ID);
                }
                foreach (var item in product.Translate)
                {
                    _translateRepository.Delete(item.ID);
                }
                _productRepository.Delete(id.Value);
                _productRepository.Save();
                _imageRepository.Save();
                _translateRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Ürün başarıyla silindi";
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index");
        }
        public IList<Category> SortCategoriesForTree(IList<Category> source, int parentId = 0, bool ignoreCategoriesWithoutExistingParent = false)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            var result = new List<Category>();

            foreach (var cat in source.Where(c => c.ParentCategoryId == parentId).ToList())
            {
                result.Add(cat);
                result.AddRange(SortCategoriesForTree(source, cat.ID, true));
            }
            if (!ignoreCategoriesWithoutExistingParent && result.Count != source.Count)
            {
                //find categories without parent in provided category source and insert them into result
                foreach (var cat in source)
                    if (result.FirstOrDefault(x => x.ID == cat.ID) == null)
                        result.Add(cat);
            }
            return result;
        }
        private string GetFormattedBreadCrumb(Category category, string separator = ">>")
        {
            string result = string.Empty;

            var breadcrumb = GetCategoryBreadCrumb(category);
            for (int i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var categoryName = breadcrumb[i].CategoryTurkishName;

                result = String.IsNullOrEmpty(result)
                    ? categoryName
                    : string.Format("{0} {1} {2}", result, separator, categoryName);
            }

            return result;
        }
        private IList<Category> GetCategoryBreadCrumb(Category category)
        {
            if (category == null)
                throw new ArgumentNullException("category");

            var result = new List<Category>();

            //used to prevent circular references
            var alreadyProcessedCategoryIds = new List<int>();

            while (category != null && //not null
                !alreadyProcessedCategoryIds.Contains(category.ID)) //prevent circular references
            {
                result.Add(category);

                alreadyProcessedCategoryIds.Add(category.ID);

                category = _categoryRepository.GetById(category.ParentCategoryId.Value);
            }
            result.Reverse();
            return result;
        }
        [HttpPost]
        public JsonResult GetImg(int Id)
        {
            var model = _productRepository.GetById(Id).Images.FirstOrDefault().Path;
            return Json(model, JsonRequestBehavior.AllowGet);
        }


    }
}