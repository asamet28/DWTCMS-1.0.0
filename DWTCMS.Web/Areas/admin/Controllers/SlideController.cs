﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.Model;
using DWTCMS.Service.Config;
using DWTCMS.Web.Areas.admin.ViewModel;
using DWTCMS.Web.Areas.admin.ViewModel.Admin.Slide;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class SlideController : Controller
    {
        private readonly ISlideRepository _slideRepository;

        public SlideController(ISlideRepository slideRepository)
        {
            _slideRepository = slideRepository;
        }
        public ActionResult Index()
        {
            var model = _slideRepository.GetAll().ToList().Select(x => new { x.ID, x.Order, x.Title, x.IsOpen, x.Path }).OrderBy(y => y.Order);

            var viewModel = new List<SlideList>();

            if (model.Any())
            {
                foreach (var p in model)
                {
                    var vm = new SlideList
                    {
                        ID = p.ID,
                        DisplayOrder = p.Order,
                        Path = p.Path,
                        Published = p.IsOpen,
                        Title = p.Title
                    };
                    viewModel.Add(vm);
                }
            }
            return View(viewModel);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SlideCreateOrEdit model, HttpPostedFileBase slideImage)
        {
            string guid = Guid.NewGuid().ToString();

            if (slideImage != null && slideImage.ContentLength > 0)
            {
                slideImage.SaveAs(Path.Combine(Server.MapPath("~/Areas/admin/Content/img/slide/"), guid + slideImage.FileName));
            }
            var cModel = new Slide
            {
                Title = model.Title,
                Path = model.Path = guid + slideImage.FileName,
                Order = model.Order,
                IsOpen = model.IsOpen
            };
            try
            {
                _slideRepository.Insert(cModel);
                _slideRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Slayt başarıyla eklendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var slide = _slideRepository.GetById(id.Value);
            if (slide == null)
            {
                return HttpNotFound();
            }
            SlideCreateOrEdit uModel = new SlideCreateOrEdit()
            {
                ID = slide.ID,
                Title = slide.Title,
                IsOpen = slide.IsOpen,
                Order = slide.Order,
                Path=slide.Path
            };
            return View(uModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SlideCreateOrEdit slide, HttpPostedFileBase slideImage)
        {
            var realSlide = _slideRepository.GetById(slide.ID);
            string guid=string.Empty;
            if (slideImage == null || slideImage.ContentLength <= 0)
            {
                slide.Path = realSlide.Path;
                if (!ModelState.IsValid)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                System.IO.File.Delete(Server.MapPath("~/Areas/admin/Content/img/slide/" + realSlide.Path));
                guid = Guid.NewGuid().ToString();
                slide.Path = guid + slideImage.FileName;
                slideImage.SaveAs(Path.Combine(Server.MapPath("~/Areas/admin/Content/img/slide/"), guid + slideImage.FileName));
            }
            Slide uModel = new Slide()
            {
                ID=slide.ID,
                Title=slide.Title,
                Order=slide.Order,
                Path=slide.Path,
                IsOpen=slide.IsOpen
            };
            try
            {
                _slideRepository.Update(uModel);
                _slideRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Slayt başarıyla düzenlendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            try
            {
                System.IO.File.Delete(Server.MapPath("~/Areas/admin/Content/img/slide/" + _slideRepository.GetById(id.Value).Path));
                _slideRepository.Delete(id.Value);
                _slideRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Slayt başarıyla silindi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult GetImg(int Id)
        {
            var model = _slideRepository.GetById(Id);
            return Json(model.Path, JsonRequestBehavior.AllowGet);
        }
    }
}