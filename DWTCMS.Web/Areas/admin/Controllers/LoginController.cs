﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Core.Repository;
using DWTCMS.Web.Areas.admin.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DWTCMS.Service.Crypt;
using DWTCMS.Data.Model;
using System.Net.Mail;
using System.Net;

namespace DWTCMS.Web.Areas.admin.Controllers
{

    public class LoginController : Controller
    {
        private readonly IAdminRepository _adminRepository;
        public LoginController(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        // GET: admin/Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(AdminModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.LoginError = true;
                return View(model);
            }
            try
            {
                var uData = new AdminRepository();
                var admin = uData.Get(x => x.Email == model.Email);
                if (admin == null)
                {
                    ViewBag.LoginError = true;
                    return View(model);
                }
                //mailden şifreyi sıfırlaması gerekiyor.
                if (admin.isChecked==false)
                {
                    ViewBag.Mail = false;
                    return View(model);
                }
                if (LoginValid(model.Email, model.Password))
                {
                    uData.Update(admin);
                    uData.Save();
                    FormsAuthentication.SetAuthCookie(model.Email, true);
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.LoginError = true;
                return View(model);
            }
            catch
            {
                ViewBag.AnError = true;
                return View(model);
            }

        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Forgot()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Forgot(AdminModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            try
            {
                var admin = _adminRepository.Get(x => x.Email.Equals(model.Email));
                if (admin==null)
                {
                    return RedirectToAction("Index");
                }
                string Guid = admin.GuidID.ToString();
                SmtpClient sc = new SmtpClient();
                sc.Port = 587;
                sc.Host = "smtp.gmail.com";
                sc.EnableSsl = true;
                sc.Credentials = new NetworkCredential("sametdemir2834@gmail.com", "123456bjk");
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("sametdemir2834@gmail.com", "Demir Web Tasarım");
                mail.To.Add(admin.Email);
                mail.Subject = "Demir Web Tasarım Şifre Sıfırlama Linki"; mail.IsBodyHtml = true; mail.Body = "<a href='http://localhost:62303/admin/Login/ConfirmMail/" + Guid + "' TARGET = '_blank'>Şifremizi Sıfırlamak İçin Tıklayınız</a>";
                sc.Send(mail);

                //login olmasını engellendi
                admin.isChecked = false;
                _adminRepository.Update(admin);
                _adminRepository.Save();
                return RedirectToAction("Index");

            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult ConfirmMail(string id)
        {
            var admin = _adminRepository.Get(x => x.GuidID.ToString().Equals(id));
            if (admin == null)
            {
                ViewBag.AnError = true;
                return RedirectToAction("Index");
            }
            var model = new ConfirmMail
            {
                Email = admin.Email
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmMail(ConfirmMail model)
        {
            try
            {
                var admin = _adminRepository.Get(x => x.Email.Equals(model.Email));
                admin.Password = BCrypt.HashPassword(model.NewPassword, BCrypt.GenerateSalt());
                admin.isChecked = true;
                admin.GuidID = Guid.NewGuid();
                _adminRepository.Update(admin);
                _adminRepository.Save();
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index");
        }


        static bool LoginValid(string Email, string password)
        {
            var isValid = false;
            var uData = new AdminRepository();
            var admin = uData.Get(x => x.Email == Email);

            if (admin != null && BCrypt.CheckPassword(password, admin.Password))
            {
                isValid = true;
            }
            return isValid;
        }
    }
}