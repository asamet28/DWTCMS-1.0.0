﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.DataContext;
using DWTCMS.Data.Model;
using DWTCMS.Service.Config;
using DWTCMS.Web.Areas.admin.Helpers;
using DWTCMS.Web.Areas.admin.ViewModel;
using DWTCMS.Web.Areas.admin.ViewModel.Admin.Category;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IProductRepository _productRepository;
        public CategoryController(ICategoryRepository categoryRepository, IProductRepository productRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
        }
        public ActionResult Index()
        {
            var model = SortCategoriesForTree(_categoryRepository.GetAll().OrderBy(x => x.ParentCategoryId).ThenBy(y => y.Order).ToList());
            var viewModel = new List<CategoryList>();
            if (model.Any())
            {
                foreach (var item in model)
                {
                    var vm = new CategoryList
                    {
                        ID = item.ID,
                        Order = item.Order,
                        IsOpen = item.IsOpen,
                        Name = GetFormattedBreadCrumb(item)
                    };
                    viewModel.Add(vm);
                }
            }
            return View(viewModel);
        }
        public ActionResult Create()
        {
            var none = new CategoryCreateOrEdit
            {
                Name = "Üst Kategorisi Yok",
                ID = 0
            };

            var model = SortCategoriesForTree(_categoryRepository.GetAll().OrderBy(x => x.ParentCategoryId).ThenBy(y => y.Order).ToList());

            var viewModel = new List<CategoryCreateOrEdit>();
            viewModel.Add(none);
            if (model.Any())
            {
                foreach (var item in model)
                {
                    var vm = new CategoryCreateOrEdit
                    {
                        ID = item.ID,
                        Order = item.Order,
                        IsOpen = item.IsOpen,
                        Name = GetFormattedBreadCrumb(item)
                    };
                    viewModel.Add(vm);
                }
            }
            object selectCategory = null;
            var selectList = new SelectList(viewModel, "Id", "Name", selectCategory);
            ViewData.Add("ParentCategoryId", selectList);
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryCreateOrEdit model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var cModel = new Category
            {
                CategoryTurkishName = model.Name,
                Order = model.Order,
                IsOpen = model.IsOpen,
                ParentCategoryId = model.ParentCategoryId,
                SeoName = model.Name.Trim().GetGlobalSeoUrlFiendlyName()
            };
            try
            {
                _categoryRepository.Insert(cModel);
                _categoryRepository.Save();

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProductHub>();
                hubContext.Clients.All.addProduct(cModel);

                TempData[AppCons.TempDataSuccess] = "Kategori başarıyla eklendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }


        public ActionResult Edit(int? id)
        {
            var none = new CategoryCreateOrEdit
            {
                Name = "Üst Kategorisi Yok",
                ID = 0
            };

            var viewModel = new List<CategoryCreateOrEdit>();
            viewModel.Add(none);

            var model = SortCategoriesForTree(_categoryRepository.GetAll().OrderBy(x => x.ParentCategoryId).ThenBy(y => y.Order).ToList());

            if (model.Any())
            {
                foreach (var item in model)
                {
                    var vm = new CategoryCreateOrEdit
                    {
                        ID = item.ID,
                        Order = item.Order,
                        IsOpen = item.IsOpen,
                        Name = GetFormattedBreadCrumb(item)
                    };
                    viewModel.Add(vm);
                }
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = _categoryRepository.GetById(id.Value);
            if (category == null)
            {
                return HttpNotFound();
            }
            CategoryCreateOrEdit uModel = new CategoryCreateOrEdit()
            {
                ID = category.ID,
                Name = category.CategoryTurkishName,
                Order = category.Order,
                IsOpen = category.IsOpen
            };
            var selectList = new SelectList(viewModel, "Id", "Name", category.ParentCategoryId);
            ViewData.Add("ParentCategoryId", selectList);
            return View(uModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryCreateOrEdit model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Category uModel = new Category()
            {
                ID = model.ID,
                CategoryTurkishName = model.Name,
                Order = model.Order,
                IsOpen = model.IsOpen,
                ParentCategoryId = model.ParentCategoryId,
                SeoName = model.Name.Trim().GetGlobalSeoUrlFiendlyName()
            };
            try
            {
                _categoryRepository.Update(uModel);
                _categoryRepository.Save();

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProductHub>();
                hubContext.Clients.All.updateProduct(uModel);

                TempData[AppCons.TempDataSuccess] = "Kategori başarıyla düzenlendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProductHub>();
                var dModel = _categoryRepository.GetById(id);
                hubContext.Clients.All.removeProduct(dModel);

                _categoryRepository.Delete(id);
                _categoryRepository.Save();

                

                TempData[AppCons.TempDataSuccess] = "Kategori başarıyla silindi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
        public IList<Category> SortCategoriesForTree(IList<Category> source, int parentId = 0, bool ignoreCategoriesWithoutExistingParent = false)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            var result = new List<Category>();

            foreach (var cat in source.Where(c => c.ParentCategoryId == parentId).ToList())
            {
                result.Add(cat);
                result.AddRange(SortCategoriesForTree(source, cat.ID, true));
            }
            if (!ignoreCategoriesWithoutExistingParent && result.Count != source.Count)
            {
                //find categories without parent in provided category source and insert them into result
                foreach (var cat in source)
                    if (result.FirstOrDefault(x => x.ID == cat.ID) == null)
                        result.Add(cat);
            }
            return result;
        }
        private string GetFormattedBreadCrumb(Category category, string separator = ">>")
        {
            string result = string.Empty;

            var breadcrumb = GetCategoryBreadCrumb(category);
            for (int i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var categoryName = breadcrumb[i].CategoryTurkishName;

                result = String.IsNullOrEmpty(result)
                    ? categoryName
                    : string.Format("{0} {1} {2}", result, separator, categoryName);
            }

            return result;
        }
        private IList<Category> GetCategoryBreadCrumb(Category category)
        {
            if (category == null)
                throw new ArgumentNullException("category");

            var result = new List<Category>();

            //used to prevent circular references
            var alreadyProcessedCategoryIds = new List<int>();

            while (category != null && //not null
                !alreadyProcessedCategoryIds.Contains(category.ID)) //prevent circular references
            {
                result.Add(category);

                alreadyProcessedCategoryIds.Add(category.ID);

                category = _categoryRepository.GetById(category.ParentCategoryId.Value);
            }
            result.Reverse();
            return result;
        }
        
    }

}
public class ProductHub : Hub
{
   
}