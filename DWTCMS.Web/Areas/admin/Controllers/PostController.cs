﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Data.Model;
using DWTCMS.Service.Config;
using DWTCMS.Web.Areas.admin.Helpers;
using DWTCMS.Web.Areas.admin.ViewModel;
using DWTCMS.Web.Areas.admin.ViewModel.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class PostController : Controller
    {
        private readonly IPostRepository _postRepository;

        public PostController(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }
        // GET: admin/Post
        public ActionResult Index()
        {
            var model = _postRepository.GetAll().Select(x => new { x.ID, x.Title, x.IsOpen, x.AddedDateTime });
            var viewModel = new List<PostList>();
            if (model.Any())
            {
                foreach (var p in model)
                {
                    var vm = new PostList
                    {
                        ID = p.ID,
                        Title = p.Title,
                        PublishDateTime = p.AddedDateTime,
                        IsOpen = p.IsOpen
                    };
                    viewModel.Add(vm);
                }
            }
            return View(viewModel);
        }
        public ActionResult Create()
        {
            ViewBag.Caption="Yeni Haber";
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(PostCreateOrEdit model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Post post = new Post() {
                Title = model.Title,
                Content = model.Content,
                SeoName = model.Title.Trim().GetGlobalSeoUrlFiendlyName()
            };
            post.AddedDateTime = DateTime.Now;
            post.IsOpen = true;
            try
            {
                _postRepository.Insert(post);
                _postRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Haber başarıyla eklendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            ViewBag.Caption = "Haber Düzenle";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _postRepository.GetById(id.Value);
            if (model == null)
            {
                return HttpNotFound();
            }
            PostCreateOrEdit uModel = new PostCreateOrEdit() {
                ID=model.ID,
                Title=model.Title,
                Content=model.Content
            };
            return View(uModel);
        }

        [ValidateInput(false)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PostCreateOrEdit model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Post uModel = new Post()
            {
                ID = model.ID,
                Title = model.Title,
                Content = model.Content,
                SeoName = model.Title.Trim().GetGlobalSeoUrlFiendlyName()
            };
            uModel.AddedDateTime=DateTime.Now;
            uModel.IsOpen=true;

            try
            {
                _postRepository.Update(uModel);
                _postRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Haber başarıyla düzenlendi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            try
            {
                _postRepository.Delete(id.Value);
                _postRepository.Save();
                TempData[AppCons.TempDataSuccess] = "Haber başarıyla silindi";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");

        }
    }
}