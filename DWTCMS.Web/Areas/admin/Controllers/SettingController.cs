﻿using DWTCMS.Service.Config;
using DWTCMS.Service.Settings;
using DWTCMS.Web.Areas.admin.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class SettingController : Controller
    {
        // GET: admin/Setting
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(SettingsModel model)
        {
            // WEB SITE SETTINGS
            AppSettings.Set(AppCons.TITLE, model.TITLE);
            AppSettings.Set(AppCons.DESCRIPTION, model.DESCRIPTION);
            AppSettings.Set(AppCons.KEYWORDS, model.KEYWORDS);
            AppSettings.Set(AppCons.HeadCode, model.HeadCode);
            AppSettings.Set(AppCons.BodyCode, model.BodyCode);
            AppSettings.Set(AppCons.NOINDEX, model.NOINDEX);

            AppSettings.Set(AppCons.FOOTER_TEXT, model.FOOTER_TEXT);
            AppSettings.Set(AppCons.CONFIRMATION_MESSAGE, model.CONFIRMATION_MESSAGE);
            AppSettings.Set(AppCons.CONFIRMATION_ISOPEN, model.CONFIRMATION_ISOPEN);

            // MAIL SETTINGS
            AppSettings.Set(AppCons.MAIL_SERVER, model.MAIL_SERVER);
            AppSettings.Set(AppCons.MAIL_USER, model.MAIL_USER);
            AppSettings.Set(AppCons.MAIL_PASSWORD, model.MAIL_PASSWORD);
            AppSettings.Set(AppCons.MAIL_SENDER_NAME, model.MAIL_SENDER_NAME);
            AppSettings.Set(AppCons.MAIL_NOTIFICATIONS, model.MAIL_NOTIFICATIONS);

            // SOCIAL SETTINGS 
            AppSettings.Set(AppCons.FACEBOOK_PAGE_URL, model.FACEBOOK_PAGE_URL);
            AppSettings.Set(AppCons.TWITTER_PAGE_URL, model.TWITTER_PAGE_URL);
            AppSettings.Set(AppCons.GOOGLE_PLUS_PAGE_URL, model.GOOGLE_PLUS_PAGE_URL);
            AppSettings.Set(AppCons.LINKEDIN_PAGE_URL, model.LINKEDIN_PAGE_URL);
            AppSettings.Set(AppCons.INSTAGRAM_PAGE_URL, model.INSTAGRAM_PAGE_URL);
            AppSettings.Set(AppCons.PINTEREST_PAGE_URL, model.PINTEREST_PAGE_URL);
            AppSettings.Set(AppCons.YOUTUBE_PAGE_URL, model.YOUTUBE_PAGE_URL);

            AppSettings.Set(AppCons.FACEBOOK_PAGE_URL_ISOPEN, model.FACEBOOK_PAGE_URL_ISOPEN);
            AppSettings.Set(AppCons.TWITTER_PAGE_URL_ISOPEN, model.TWITTER_PAGE_URL_ISOPEN);
            AppSettings.Set(AppCons.LINKEDIN_PAGE_URL_ISOPEN, model.LINKEDIN_PAGE_URL_ISOPEN);
            AppSettings.Set(AppCons.GOOGLE_PLUS_PAGE_URL_ISOPEN, model.GOOGLE_PLUS_PAGE_URL_ISOPEN);
            AppSettings.Set(AppCons.INSTAGRAM_PAGE_URL_ISOPEN, model.INSTAGRAM_PAGE_URL_ISOPEN);
            AppSettings.Set(AppCons.PINTEREST_PAGE_URL_ISOPEN, model.PINTEREST_PAGE_URL_ISOPEN);
            AppSettings.Set(AppCons.YOUTUBE_PAGE_URL_ISOPEN, model.YOUTUBE_PAGE_URL_ISOPEN);


            AppSettings.Set(AppCons.ADRES, model.ADRES);
            AppSettings.Set(AppCons.TELEFON, model.TELEFON);
            AppSettings.Set(AppCons.MAIL, model.MAIL);
            AppSettings.Set(AppCons.FAX, model.FAX);
            AppSettings.Set(AppCons.SAATLER, model.SAATLER);


            AppSettings.Set(AppCons.FacebookAppSecret, model.FacebookAppSecret);
            AppSettings.Set(AppCons.FacebookAppId, model.FacebookAppId);
            AppSettings.Set(AppCons.FacebookAppScope, model.FacebookAppScope);

            AppSettings.Set(AppCons.reCAPTCHA_SiteKey, model.reCAPTCHA_SiteKey);
            AppSettings.Set(AppCons.reCAPTCHA_SecretKey, model.reCAPTCHA_SecretKey);


            //if (model.deletePanelLogo)
            //{
            //    AppCons.PANEL_LOGO.DeleteFile();
            //    AppSettings.Set(AppCons.PANEL_LOGO, null);
            //}
            //else
            //{
            //    if (!String.IsNullOrEmpty(model.PANEL_LOGO))
            //    {
            //        AppSettings.Set(AppCons.PANEL_LOGO, model.PANEL_LOGO);
            //    }
            //}

            TempData[AppCons.TempDataSuccess] = "Ayarlar kaydedildi";
            return View();
        }
        public class SettingsModel
        {
            public string NOINDEX { get; set; }
            public string HeadCode { get; set; }
            public string BodyCode { get; set; }

            public string FacebookAppSecret { get; set; }
            public string FacebookAppId { get; set; }
            public string FacebookAppScope { get; set; }


            public string reCAPTCHA_SiteKey { get; set; }
            public string reCAPTCHA_SecretKey { get; set; }


            public string TITLE { get; set; }
            public string SAATLER { get; set; }
            public string FAX { get; set; }
            public string DESCRIPTION { get; set; }
            public string KEYWORDS { get; set; }
            [AllowHtml]
            public string GOOGLE_ANALYTICS_CODE { get; set; }
            public string FOOTER_TEXT { get; set; }
            public string MAIL_SERVER { get; set; }
            public string MAIL_USER { get; set; }
            public string MAIL_PASSWORD { get; set; }
            public string MAIL_SENDER_NAME { get; set; }
            public string MAIL_NOTIFICATIONS { get; set; }
            public string FACEBOOK_PAGE_URL { get; set; }
            public string TWITTER_PAGE_URL { get; set; }
            public string LINKEDIN_PAGE_URL { get; set; }
            public string GOOGLE_PLUS_PAGE_URL { get; set; }
            public string FACEBOOK_PAGE_URL_ISOPEN { get; set; }
            public string TWITTER_PAGE_URL_ISOPEN { get; set; }
            public string LINKEDIN_PAGE_URL_ISOPEN { get; set; }
            public string GOOGLE_PLUS_PAGE_URL_ISOPEN { get; set; }
            public string CONFIRMATION_MESSAGE { get; set; }
            public string CONFIRMATION_ISOPEN { get; set; }
            public string INSTAGRAM_PAGE_URL { get; set; }
            public string PINTEREST_PAGE_URL { get; set; }
            public string INSTAGRAM_PAGE_URL_ISOPEN { get; set; }
            public string PINTEREST_PAGE_URL_ISOPEN { get; set; }
            public string YOUTUBE_PAGE_URL_ISOPEN { get; set; }
            public string YOUTUBE_PAGE_URL { get; set; }
            public string PANEL_LOGO { get; set; }
            public bool deletePanelLogo { get; set; }
            public string ADRES { get; set; }
            public string TELEFON { get; set; }
            public string MAIL { get; set; }


        }
    }
}