﻿using DWTCMS.Core.Infrastructure;
using DWTCMS.Web.Areas.admin.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace DWTCMS.Web.Areas.admin.Controllers
{
    [LoginControl]
    public class HomeController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ISlideRepository _slideRepository;
        private readonly IPostRepository _postRepository;

        public HomeController(IProductRepository productRepository,ISlideRepository slideRepository,IPostRepository postRepository)
        {
            _productRepository = productRepository;
            _slideRepository = slideRepository;
            _postRepository = postRepository;
        }
        public ActionResult Index()
        {
            var pageModel = new HomePageModel
            {
                ProductCount = _productRepository.Count(),
                SlideCount = _slideRepository.Count(),
                PostCount = _postRepository.Count(),


            };

            return View(pageModel);
        }

    }
}