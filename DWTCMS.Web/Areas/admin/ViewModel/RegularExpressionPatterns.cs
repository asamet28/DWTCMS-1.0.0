﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel
{
    public class RegularExpressionPatterns
    {
        public const string SecurityCode = @"^[a-zA-Z0-9]{6}$";
        public const string Iban = @"^[TR0-9 ]{32}$";
        public const string TaxOfficeNo = @"^[0-9]{10}$";
        public const string Username = @"^[a-zA-Z0-9-]{3,20}$";
        public const string TrName = @"^[a-zA-ZöçşığüÖÇŞİĞÜ\s]*";
        public const string UrlOrNull = @"(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w-]*)?|null";
        public const string Email = @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
    }
}