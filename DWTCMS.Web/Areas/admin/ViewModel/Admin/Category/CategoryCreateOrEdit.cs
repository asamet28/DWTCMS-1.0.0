﻿using DWTCMS.Service.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.Category
{
    public class CategoryCreateOrEdit
    {
        public int ID { get; set; }

        [Display(Name = "Ad")]
        public string Name { get; set; }
        
        [Display(Name = "Sırası")]
        [Required(ErrorMessage = AppCons.Required)]
        public int Order { get; set; }

        [Display(Name = "Görüntülensinmi?")]
        public bool IsOpen { get; set; }

        [Display(Name="Üst Kategori")]
        public int? ParentCategoryId { get; set; }

        [Display(Name = "Açıklama")]
        public string MetaDescription { get; set; }

        [Display(Name = "Kelimeler")]
        public string MetaKeyword { get; set; }
    }
}