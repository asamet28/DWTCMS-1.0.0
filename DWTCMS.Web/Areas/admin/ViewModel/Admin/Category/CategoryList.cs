﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.Category
{
    public class CategoryList
    {
        public int ID { get; set; }

        [Display(Name = "Ad")]
        public string Name { get; set; }

        [Display(Name = "Sırası")]
        public int Order { get; set; }

        [Display(Name = "Görüntülensinmi?")]
        public bool IsOpen { get; set; }
    }
}