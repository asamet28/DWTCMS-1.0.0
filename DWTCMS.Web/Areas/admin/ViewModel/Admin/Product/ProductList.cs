﻿using DWTCMS.Data.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.Product
{
    public class ProductList
    {
        public int ID { get; set; }

        [Display(Name = "Ad")]
        public string TName { get; set; }

        [Display(Name = "Kod")]
        public string Code { get; set; }

        [Display(Name = "Kategori")]
        public string Category { get; set; }

        [Display(Name = "Sırası")]
        public int Order { get; set; }

        [Display(Name = "Görüntülensinmi?")]
        public bool IsOpen { get; set; }

        [Display(Name = "Resim")]
        public string ImgPath { get; set; }
    }
}