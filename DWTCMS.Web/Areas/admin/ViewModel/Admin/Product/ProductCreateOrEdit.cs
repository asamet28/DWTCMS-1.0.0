﻿using DWTCMS.Service.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.Product
{
    public class ProductCreateOrEdit
    {
        public int ID { get; set; }

        [Display(Name = "Ad")]
        [Required(ErrorMessage = AppCons.Required)]
        public string trName { get; set; }

        [Display(Name = "Ad")]
        public string enName { get; set; }

        [Display(Name = "Kod")]
        public string Code { get; set; }

        [Display(Name = "Kategori")]
        public int ParentCategoryId { get; set; }

        [Display(Name = "Sırası")]
        [Required(ErrorMessage = AppCons.Required)]
        public int Order { get; set; }

        [Display(Name = "Görüntülensinmi?")]
        public bool IsOpen { get; set; }

        [Display(Name = "Resim")]
        public string ImgPath { get; set; }

        [Display(Name = "Açıklama")]
        public string MetaDescription { get; set; }

        [Display(Name = "Kelimeler")]
        public string MetaKeyword { get; set; }
    }
   
}