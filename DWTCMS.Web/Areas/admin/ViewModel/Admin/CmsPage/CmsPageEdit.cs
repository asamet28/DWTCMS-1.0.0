﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.CmsPage
{
    public class CmsPageEdit
    {
        public int ID { get; set; }

        [Display(Name = "Kısa Açıklama")]
        public string PageShortDescription { get; set; }
        [DataType(DataType.MultilineText)]

        [Display(Name = "İçerik")]
        public string PageDescription { get; set; }

        public int PageValue { get; set; }

        [Display(Name = "Kelimeler")]
        public string MetaKeyword { get; set; }
    }
}