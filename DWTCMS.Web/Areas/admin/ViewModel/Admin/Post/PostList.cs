﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Post
{
    public class PostList
    {
        public int ID { get; set; }

        [Display(Name = "Başlık")]
        public string Title { get; set; }

        [Display(Name = "Görüntülensinmi?")]
        public bool IsOpen { get; set; }

        [Display(Name = "Kayıt Tarihi")]
        public DateTime? PublishDateTime { get; set; }
    }
}