﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Post
{
    public class PostCreateOrEdit
    {
        public int ID { get; set; }

        [Display(Name = "Başlık")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "İçerik")]
        public string Content { get; set; }

        [Display(Name = "Açıklama")]
        public string MetaDescription { get; set; }

        [Display(Name = "Kelimeler")]
        public string MetaKeyword { get; set; }
    }
}