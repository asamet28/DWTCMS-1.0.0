﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.Slide
{
    public class SlideList
    {
        public int ID { get; set; }
        [Display(Name = "Başlık")]
        public string Title { get; set; }
        [Display(Name = "Görüntülensinmi?")]
        public bool Published { get; set; }
        [Display(Name = "Sırası")]
        public int DisplayOrder { get; set; }
        [Display(Name = "Resim")]
        public string Path { get; set; }
    }
}