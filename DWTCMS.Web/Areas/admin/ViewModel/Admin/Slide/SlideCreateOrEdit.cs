﻿using DWTCMS.Service.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel.Admin.Slide
{
    public class SlideCreateOrEdit
    {
        public int ID { get; set; }

        [Display(Name = "Başlık")]
        public string Title { get; set; }

        [Display(Name = "Görüntülensinmi?")]
        public bool IsOpen { get; set; }

        [Display(Name = "Sırası")]
        [Required(ErrorMessage = AppCons.Required)]
        public int Order { get; set; }

        [Display(Name = "Resim")]
        public string Path { get; set; }
    }
}