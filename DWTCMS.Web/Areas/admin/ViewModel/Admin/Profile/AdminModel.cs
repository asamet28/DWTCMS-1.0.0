﻿using DWTCMS.Service.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel
{
    public class AdminModel
    {
        [Required]
        [RegularExpression(RegularExpressionPatterns.Email, ErrorMessage = "{0} geçersiz")]
        [StringLength(100, ErrorMessage = "{0} en fazla {1} karakterden oluşabilir.")]
        public string Email { get; set; }
        public string Password { get; set; }
    }
}