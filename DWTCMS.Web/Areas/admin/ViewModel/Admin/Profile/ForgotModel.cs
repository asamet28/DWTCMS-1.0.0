﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel
{
    public class ForgotModel
    {
        public string Email { get; set; }
    }
}