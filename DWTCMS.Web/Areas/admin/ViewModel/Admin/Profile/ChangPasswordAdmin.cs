﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel
{
    public class ChangPasswordAdmin
    {
        [Required(ErrorMessage = "Bu alan gereklidir")]
        [Display(Name = "Mevcut Şifreniz")]
        public string Password { get; set; }

        [Display(Name = "Yeni Şifre")]
        [Required(ErrorMessage = "Bu alan gereklidir")]
        public string NewPassword { get; set; }

        [Display(Name = "Yeni Şifre Tekrar")]
        [Required(ErrorMessage = "Bu alan gereklidir")]
#pragma warning disable 618
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "Yeni şifre ve tekrarı eşleşmiyor")]
#pragma warning restore 618
        public string ConfirmPassword { get; set; }
    }
}