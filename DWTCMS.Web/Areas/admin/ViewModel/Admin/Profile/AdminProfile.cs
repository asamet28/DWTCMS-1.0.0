﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace DWTCMS.Web.Areas.admin.ViewModel.Admin
{
    public class AdminProfile
    {
        [Display(Name = "Kullanıcı adı")]
        [Required]
        [StringLength(100, ErrorMessage = "{0} en fazla {1} karakterden oluşabilir.")]
        public string Name { get; set; }

        [Display(Name = "Eposta")]
        [Required]
        [RegularExpression(RegularExpressionPatterns.Email, ErrorMessage = "{0} geçersiz")]
        [StringLength(100, ErrorMessage = "{0} en fazla {1} karakterden oluşabilir.")]
        public string Email { get; set; }
    }
}