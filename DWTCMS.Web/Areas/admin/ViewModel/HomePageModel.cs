﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Areas.admin.ViewModel
{
    public class HomePageModel
    {
        public int CategoryCount { get; set; }
        public int ProductCount { get; set; }
        public int SlideCount { get; set; }
        public int GalleryCount { get; set; }
        public int GalleryCategoryCount { get; set; }

        public int CorporateCount { get; set; }
        public int SettingsCount { get; set; }
        public int CatalogCount { get; set; }
        public int ContactCount { get; set; }
        public int PostCount { get; set; }
    }
}