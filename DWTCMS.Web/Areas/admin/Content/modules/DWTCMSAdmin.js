﻿
$(document).ready(function () {
    //begin tags input ta entera basınsa sayfanın post olmasını engeller.
    $('#tagsinput').tagsinput({
        confirmKeys: [13, 44]
    });
    $('.bootstrap-tagsinput input').on('keypress', function (e) {
        if (e.keyCode == 13) {
            e.keyCode = 188;
            e.preventDefault();
        };
    });
    //end

    //begin product index sayfasında resim simgesinin üzerine gelince ajax ile resim gösterir
    $(".hoverImgProduct").hover(function () {
        var ID = $(this).data('id');
        $.ajax({
            url: '/Product/GetImg/' + ID,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('[data-id=' + ID + ']').addClass("deneme");
                $('.deneme img').removeClass("hidden");
                $('.deneme img').attr('src', '../../Areas/admin/Content/img/product/' + data);
            },
            error: function (hata, ajaxOptions, thrownError) {
            }
        });
    }, function () {
        $('.deneme img').addClass("hidden");
        $('.deneme img').attr('src', "");
        $('.deneme').removeClass("deneme");
    });
    //end

    //begin slide index sayfasında resim simgesinin üzerine gelince ajax ile resim gösterir
    $(".hoverImgSlide").hover(function () {
        var ID = $(this).data('id');
        $.ajax({
            url: '/Slide/GetImg/' + ID,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('[data-id=' + ID + ']').addClass("deneme");
                $('.deneme img').removeClass("hidden");
                $('.deneme img').attr('src', '../../Areas/admin/Content/img/slide/' + data);
            },
            error: function (hata, ajaxOptions, thrownError) {
            }
        });
    }, function () {
        $('.deneme img').addClass("hidden");
        $('.deneme img').attr('src', "");
        $('.deneme').removeClass("deneme");
    });
    //end

    //begin index sayfalarında sil butonuna basınca çıkan modal
    $('.sweet-4').on('click', function () {
        var title = $(this).data('title');
        var id = $(this).data('id');
        $('#id').val(id);
        swal({
            title: title,
            text: "Silmek istediğinize emin misiniz?",
            type: "info",
            showCancelButton: true,
            cancelButtonText: "İptal",
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Sil',
            closeOnConfirm: true,
        },
        function () {
            $('#deleteForm').trigger('submit');
        });
    });
    //end

    //begin table düzenlemesi
    $('#myTable').DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "Kayıt Yok",
            //"info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "info": "_TOTAL_ Adet Kayıttan _START_ ile _END_ Arasındaki Kayıtlar Gösteriliyor",
            //"infoEmpty": "Showing 0 to 0 of 0 entries",
            "infoEmpty": "Kayıt Yok",
            //"infoFiltered": "(filtered from _MAX_ total entries)",
            "infoFiltered": "(_MAX_ Adet Kayıttan Filtrelendi)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "_MENU_ Adet Kayıt",
            "loadingRecords": "Bağlanıyor...",
            "processing": "İşleniyor...",
            "search": "Ara:",
            "zeroRecords": "Eşleşen Kayıt Bulunamadı",
            "paginate": {
                "first": "İlk",
                "last": "Son",
                "next": "İleri",
                "previous": "Geri"
            }
        }
    });
    //end
});