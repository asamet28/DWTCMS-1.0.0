﻿var dropzone = function () {
    var siteUrl = "";



    return {

        init: function (input, maxWidth, maxHeight, maxWidth2, maxHeight2) {

            siteUrl = window.location.protocol + "//" + window.location.host + vpath;

            Dropzone.options.uploader = {

                url: '/admin/adminuploader/upload',
                previewsContainer: ".dropzone-previews",
                uploadMultiple: false,
                parallelUploads: 1,
                maxFiles: 1,
                autoProcessQueue: true,
                clickable: true,
                maxFilesize: '100', // MB
                acceptedFiles: ".gif, .jpeg, .jpg, .png,.GIF,.JPG,.JPEG,.PNG",
                addRemoveLinks: false,
                sending: function (file, xhr, formData) {
                    // Pass token. You can use the same method to pass any other values
                    //as well such as a id to associate the image with for example.
                    formData.append("__RequestVerificationToken", $('input[name=__RequestVerificationToken]').val());
                    // Laravel expect the token post value to be named _token by default

                    // Scale block item images
                    if (maxWidth !== undefined && maxHeight !== undefined) {
                        formData.append("maxWidth", maxWidth);
                        formData.append("maxHeight", maxHeight);
                    }
                },
                init: function () {
                    var myDropzone = this;
                    this.on("success", function (files, response) {
                        var sd = response.toString().split(',');
                        $(input).val(sd[0]);
                        $('.slidethumbnail img')
                            .attr('src', siteUrl + sd[0])
                            .attr('href', siteUrl + sd[0]);
                        SesliKose.initImagePopup($('.slidethumbnail img'));
                        $('.gorselKaldir').show();
                        myDropzone.removeAllFiles();
                    });
                }
            };


            $('.gorselKaldir').on('click', function () {
                SesliKose.confirm("görseli", function (result) {
                    if (result) {
                        $.post('/admin/adminuploader/removeuploadedfile', {
                            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                            _: $(input).val()
                        }, function () {
                            $(input).val('');
                            $('.slidethumbnail img').attr('src', '').attr('href', '').attr('rel', '').unbind('click');
                            $('.gorselKaldir').hide();
                        });
                    }
                });
            });
        }

    };
}();