﻿using System.Web.Mvc;

namespace DWTCMS.Web.Areas.admin
{
    public class adminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
              name: "ConfirmJewelleryLogin",
              url: "admin/ConfirmJewelleryLogin/{guid}",
              defaults: new { controller = "Login", action = "ConfirmJewelleryLogin", guid = UrlParameter.Optional }
              );

            context.MapRoute(
                "admin_default",
                "admin/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}