﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DWTCMS.Web.Models
{
    public class HomeProduct
    {
        public string trName { get; set; }
        public string MetaDescription { get; set; }
        public string Code { get; set; }
    }
}