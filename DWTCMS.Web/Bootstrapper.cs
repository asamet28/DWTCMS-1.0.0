﻿using Autofac;
using Autofac.Integration.Mvc;
using DWTCMS.Core.Infrastructure;
using DWTCMS.Core.Repository;
using DWTCMS.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DWTCMS.Web
{
    public static class Bootstrapper
    {
        public static void RunConfig()
        {
            BuildAutofac();
        }

        private static void BuildAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<AdminRepository>().As<IAdminRepository>();
            builder.RegisterType<PostRepository>().As<IPostRepository>();
            builder.RegisterType<TranslateRepository>().As<ITranslateRepository>();
            builder.RegisterType<CmsPageRepository>().As<ICmsPageRepository>();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();
            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<SlideRepository>().As<ISlideRepository>();
            builder.RegisterType<ImageRepository>().As<IImageRepository>();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}