USE [DWTCMSDb]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [CategoryTurkishName], [CategoryEnglishName], [CategoryRussianName], [CategoryArabicName], [Order], [IsOpen], [SeoName], [MetaDescription], [MetaKeyword], [ParentCategoryId]) VALUES (1, N'Kare', NULL, NULL, NULL, 1, 1, N'kare', NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [Code], [ProductTurkishName], [ProductEnglishName], [ProductRussianName], [ProductArabicName], [CategoryId], [IsOpen], [Order], [SeoName], [MetaDescription], [MetaKeyword]) VALUES (1, N'aa', N'Web Sitesi', NULL, NULL, NULL, 1, 1, 1, N'web-sitesi', N'dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500''lerden beri endüstri standardı sahte metinler olarak kullanılmıştır.', N'aa')
INSERT [dbo].[Product] ([ID], [Code], [ProductTurkishName], [ProductEnglishName], [ProductRussianName], [ProductArabicName], [CategoryId], [IsOpen], [Order], [SeoName], [MetaDescription], [MetaKeyword]) VALUES (8, N'cc', N'Mobil Uygulama', NULL, NULL, NULL, 1, 1, 2, N'mobil-uygulama-1', N'dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500''lerden beri endüstri standardı sahte metinler olarak kullanılmıştır.', NULL)
INSERT [dbo].[Product] ([ID], [Code], [ProductTurkishName], [ProductEnglishName], [ProductRussianName], [ProductArabicName], [CategoryId], [IsOpen], [Order], [SeoName], [MetaDescription], [MetaKeyword]) VALUES (9, N'bb', N'Hosting', NULL, NULL, NULL, 1, 1, 3, N'hosting-1', N'dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500''lerden beri endüstri standardı sahte metinler olarak kullanılmıştır.', NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[CmsPage] ON 

INSERT [dbo].[CmsPage] ([ID], [PageTitle], [PageShortDescription], [PageDescription], [Image], [PageValue], [MetaKeyword]) VALUES (3, NULL, N'ShortAbout', N'<h2>Nereden Bulabilirim?</h2>
<p>Lorem Ipsum pasajlarının birçok çeşitlemesi vardır. Ancak bunların büyük bir çoğunluğu mizah katılarak veya rastgele sözcükler eklenerek değiştirilmişlerdir. Eğer bir Lorem Ipsum pasajı kullanacaksanız, metin aralarına utandırıcı sözcükler gizlenmediğinden emin olmanız gerekir. İnternet''teki tüm Lorem Ipsum üreteçleri önceden belirlenmiş metin bloklarını yineler. Bu da, bu üreteci İnternet üzerindeki gerçek Lorem Ipsum üreteci yapar. Bu üreteç, 200''den fazla Latince sözcük ve onlara ait cümle yapılarını içeren bir sözlük kullanır. Bu nedenle, üretilen Lorem Ipsum metinleri yinelemelerden, mizahtan ve karakteristik olmayan sözcüklerden uzaktır.</p>
', NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[CmsPage] OFF
SET IDENTITY_INSERT [dbo].[Post] ON 

INSERT [dbo].[Post] ([ID], [Title], [Content], [AddedDateTime], [IsOpen], [Order], [SeoName], [MetaDescription], [MetaKeyword]) VALUES (2, N'title', N'content', CAST(N'2016-05-16 13:31:07.267' AS DateTime), 1, 1, N'sem', NULL, N'deneme,test,tag')
INSERT [dbo].[Post] ([ID], [Title], [Content], [AddedDateTime], [IsOpen], [Order], [SeoName], [MetaDescription], [MetaKeyword]) VALUES (3, N'title-2', N'içerik', CAST(N'2016-05-16 13:31:07.000' AS DateTime), 1, 2, N'sem', NULL, NULL)
INSERT [dbo].[Post] ([ID], [Title], [Content], [AddedDateTime], [IsOpen], [Order], [SeoName], [MetaDescription], [MetaKeyword]) VALUES (4, N'title-3', N'<p>
	content-3</p>
', CAST(N'2017-03-10 17:55:07.670' AS DateTime), 1, 0, N'title-3', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Post] OFF
SET IDENTITY_INSERT [dbo].[Slide] ON 

INSERT [dbo].[Slide] ([ID], [Title], [Path], [ImageName], [IsOpen], [Order], [MetaDescription], [MetaKeyword]) VALUES (1, N'Slide-1', N'b0caf31c-1951-4b0d-bb75-551a6e4d7805web-design-2038872_960_720.jpg', NULL, 1, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Slide] OFF
SET IDENTITY_INSERT [dbo].[Translate] ON 

INSERT [dbo].[Translate] ([ID], [LanguageCode], [Name], [MetaDescription], [MetaKeyword], [ProductID], [CategoryID], [PostID], [SlideID], [CmsPageID]) VALUES (1, 0, N'ccc', NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Translate] ([ID], [LanguageCode], [Name], [MetaDescription], [MetaKeyword], [ProductID], [CategoryID], [PostID], [SlideID], [CmsPageID]) VALUES (15, 0, N'cc', NULL, NULL, 8, NULL, NULL, NULL, NULL)
INSERT [dbo].[Translate] ([ID], [LanguageCode], [Name], [MetaDescription], [MetaKeyword], [ProductID], [CategoryID], [PostID], [SlideID], [CmsPageID]) VALUES (16, 0, N'bb', NULL, NULL, 9, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Translate] OFF
SET IDENTITY_INSERT [dbo].[Image] ON 

INSERT [dbo].[Image] ([ID], [ImageName], [ContentType], [Path], [ProductID]) VALUES (16, NULL, NULL, N'b0f02595-cbaf-4c5e-b0a4-91b2997b9c63web.jpg', 1)
INSERT [dbo].[Image] ([ID], [ImageName], [ContentType], [Path], [ProductID]) VALUES (17, NULL, NULL, N'3e3f600e-caba-4c61-bc8c-957c946d1894web.jpg', 8)
INSERT [dbo].[Image] ([ID], [ImageName], [ContentType], [Path], [ProductID]) VALUES (18, NULL, NULL, N'76eea034-204a-44fb-9bf2-69471fa72e32web.jpg', 9)
SET IDENTITY_INSERT [dbo].[Image] OFF
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([ID], [Name], [SurName], [Password], [Email], [isChecked], [GuidID]) VALUES (1, N'Samet', N'Demir', N'$2a$10$TP8aN0C.8nV0TqQxbAuBzeBQIh5TatWTQXXGDgY0Yw9PpGBCPmecm', N'info@demirwebtasarim.net', 1, N'b0f02595-cbaf-4c5e-b0a4-91b2997b9c63')
SET IDENTITY_INSERT [dbo].[Admin] OFF
